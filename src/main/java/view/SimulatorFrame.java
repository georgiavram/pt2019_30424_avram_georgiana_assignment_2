package view;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Graphics;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Date;
import java.util.concurrent.ThreadLocalRandom;

import javax.swing.BorderFactory;
import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextField;
import javax.swing.border.Border;
import javax.swing.plaf.OptionPaneUI;

import concurrency.Scheduler;
import concurrency.ServerQueue;
import concurrency.SimulationManager;
import model.Task;

public class SimulatorFrame extends JFrame implements ActionListener {

	private static final long serialVersionUID = 1L;
	protected JFrame frame = new JFrame("Queue Management - Assignment 2");
	protected JPanel content = new JPanel();
	protected JPanel header = new JPanel();
	protected JPanel body = new JPanel();
	protected JScrollPane scrollPanel = new JScrollPane();
	protected JList<ServerObj> servers = new JList<ServerObj>();
	protected ArrayList<ServerObj> serversList = new ArrayList<ServerObj>();
	private int WIDTH = 800, HEIGHT = 600;
	// Labels
	protected JLabel l1 = new JLabel("Number of queues: ");
	protected JLabel l2 = new JLabel("Interval of arriving: ");
	protected JLabel l3 = new JLabel("Interval service time: ");
	protected JLabel l4 = new JLabel("Service available time: ");
	protected JLabel l5 = new JLabel("Opening time: ");
	// TextFields
	protected JTextField numberQueues = new JTextField(15);
	protected JTextField openingTime = new JTextField(5);
	protected JTextField minArriving = new JTextField(5);
	protected JTextField maxArriving = new JTextField(5);
	protected JTextField minService = new JTextField(5);
	protected JTextField maxService = new JTextField(5);
	protected JTextField serviceAvailable = new JTextField(5);
	// Buttons
	protected JButton start_btn = new JButton("Start");

	/**
	 * 
	 */
	public SimulatorFrame() {

//		this.add(content);

		this.frame.setSize(WIDTH, HEIGHT);
		this.frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		this.frame.setLocationRelativeTo(null);
//		frame.setBounds(600, 600, 600, 600);
		this.frame.getContentPane().setLayout(null);
		this.frame.setVisible(false);

		this.content.setLayout(new BoxLayout(this.content, BoxLayout.Y_AXIS));
		this.header.setBorder(BorderFactory.createEmptyBorder(20, 20, 20, 600));
		this.body.setLayout(new BoxLayout(this.body, BoxLayout.LINE_AXIS));
		this.body.setBounds(600, 0, 600, 600);
		this.body.setBorder(BorderFactory.createEmptyBorder(20, 20, 20, 20));
//		this.servers.setLayout(new BoxLayout(this.servers, BoxLayout.X_AXIS));

		this.numberQueues.setText("3");
		this.openingTime.setText("8:00");
		this.minArriving.setText("1");
//		this.minArrivingM.setText("0");
		this.maxArriving.setText("5");
//		this.maxArrivingM.setText("0");
		this.minService.setText("1");
		this.maxService.setText("10");
		this.serviceAvailable.setText("60");

		this.header.add(l1);
		this.header.add(numberQueues);
		this.header.add(l5);
		this.header.add(openingTime);
		this.header.add(l2);
		this.header.add(minArriving);
//		this.header.add(minArrivingM);
		this.header.add(maxArriving);
//		this.header.add(maxArrivingM);

		this.header.add(l3);
		this.header.add(minService);
		this.header.add(maxService);
		this.header.add(l4);
		this.header.add(serviceAvailable);
		
		Color c = new Color(173, 216, 230);
		start_btn.setBackground(Color.white);
		this.header.add(start_btn);
		header.setBackground(c);
		body.setBackground(c);
		content.setBackground(c);
		this.start_btn.addActionListener(this);
		this.content.add(this.header);

//		this.servers.setListData(this.serversList.toArray(new ServerObj[this.serversList.size()]));
//		this.servers.setLayoutOrientation(JList.HORIZONTAL_WRAP);
//		this.scrollPanel.add(this.servers);
//		this.body.add(this.servers);

		this.content.add(this.body);
		// this.add(content);
		this.frame.setContentPane(this.content);
		this.frame.setVisible(true);
	}

	/* (non-Javadoc)
	 * @see java.awt.event.ActionListener#actionPerformed(java.awt.event.ActionEvent)
	 */
	public void actionPerformed(ActionEvent e) {

		if (e.getSource() == start_btn) {
			if (!this.numberQueues.getText().isEmpty() && !this.minArriving.getText().isEmpty()
					&& !this.maxArriving.getText().isEmpty() && !this.minService.getText().isEmpty()
					&& !this.maxService.getText().isEmpty() && !this.serviceAvailable.getText().isEmpty()
					&& !this.openingTime.getText().isEmpty() && this.openingTime.getText().split(":").length == 2) {
				numberQueues.setEditable(false);
				minArriving.setEditable(false);
				maxArriving.setEditable(false);
				minService.setEditable(false);
				maxService.setEditable(false);
				Scheduler schedule = new Scheduler(Integer.parseInt(numberQueues.getText()), this);
				SimulationManager sManager = new SimulationManager(schedule, this);
				Thread t = new Thread(sManager);
				t.start();

			} else {
				JOptionPane.showMessageDialog(this.content, "Fill all the fields with valid data.");
			}
		}
	}

	/**
	 * @param server
	 * @param index
	 */
	public void addServer(ServerObj server, int index) {

		this.serversList.add(server);
//		this.servers.setListData(this.serversList.toArray(new ServerObj[this.serversList.size()]));
//		this.body.removeAll();

		this.body.add(server);
//		if(index < Integer.parseInt(numberQueues.getText())) {
//			this.body.add(Box.createHorizontalGlue());
//		}
		this.body.revalidate();
		this.body.repaint();

	}

	/**
	 * @param server
	 * @param index
	 */
	public void updateSever(ServerObj server, int index) {
		if (this.serversList.size() > 0) {
			this.serversList.set(index, server);
		}
		
//		this.body.add(server, index);
//		this.servers.setListData(this.serversList.toArray(new ServerObj[this.serversList.size()]));
		this.body.removeAll();
		for (int i = 0; i < this.serversList.size(); i++) {

			this.body.add(this.serversList.get(i));
//			if(i < this.serversList.size()) {
//				this.body.add(Box.createHorizontalGlue());
//			}
		}
		this.body.revalidate();
		this.body.repaint();
	}

	/**
	 * @param index
	 */
	public void closeServer(int index) {
		this.body.removeAll();
		for (int i = 0; i < this.serversList.size(); i++) {
			ServerObj thisServer = this.serversList.get(i);
			if (index == i)
				thisServer.setBackground(Color.red);
			this.body.add(thisServer);
		}
		this.body.revalidate();
		this.body.repaint();
	}


	/**
	 * @return
	 */
	public int GetOpeningTime() {
		int hours = Integer.parseInt(this.openingTime.getText().split(":")[0]);
		int minutes = Integer.parseInt(this.openingTime.getText().split(":")[1]);
		return (hours * 60) + minutes;
	}

	/**
	 * @return
	 */
	public int GetMinArriving() {
//		try {
		return Integer.parseInt(this.minArriving.getText());
//		}
//		catch(NumberFormatException exc) {
//			JOptionPane.showMessageDialog(this, "bad input");
//		}
//		return 0;
	}

	/**
	 * @return
	 */
	public int GetMaxArriving() {
		return Integer.parseInt(this.maxArriving.getText());
	}

	/**
	 * @return
	 */
	public int GetMinService() {
		return Integer.parseInt(this.minService.getText());
	}

	/**
	 * @return
	 */
	public int GetMaxService() {
		return Integer.parseInt(this.maxService.getText());
	}

	/**
	 * @return
	 */
	public int GetServiceAvailable() {
		return Integer.parseInt(this.serviceAvailable.getText());
	}
}
