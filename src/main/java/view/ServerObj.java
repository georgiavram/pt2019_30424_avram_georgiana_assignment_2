package view;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.util.ArrayList;

import javax.swing.BorderFactory;
import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.JLabel;
import javax.swing.JPanel;

import model.Task;

public class ServerObj extends JPanel {
	private static final long serialVersionUID = 1L;
	protected JLabel serverName = new JLabel();
	protected JPanel tasksPanel = new JPanel();
	private int noOfTasks;


	/**
	 * @param name
	 */
	public ServerObj(String name) {
		Color c = new Color(173, 216, 230);
		tasksPanel.setBackground(c);
		this.setLayout(new BoxLayout(this, BoxLayout.PAGE_AXIS));
		this.setMinimumSize(new Dimension(50, 100));
		this.setPreferredSize(new Dimension(50, 100));
		this.setBackground(Color.green);
		this.serverName.setText(name);
		this.add(this.serverName, BorderLayout.CENTER);
		this.tasksPanel.setLayout(new BoxLayout(this.tasksPanel, BoxLayout.PAGE_AXIS));
		//this.tasksPanel.add(Box.createVerticalGlue());
		this.tasksPanel.setBorder(BorderFactory.createEmptyBorder(10, 10, 10, 10));
		this.tasksPanel.setSize(200, 300);
		this.tasksPanel.setMinimumSize(new Dimension(80, 100));
		this.tasksPanel.setPreferredSize(new Dimension(80, 100));
		//this.tasksPanel.setBounds(600, 0, 600, 600);
		this.add(Box.createHorizontalGlue());
		this.add(this.tasksPanel, BorderLayout.NORTH);
		//this.add(Box.createHorizontalGlue());
	}

	/**
	 * @param task
	 * @return
	 */
	public int AddTask(Task task) {
		int index = this.tasksPanel.getComponentCount();
		JLabel thisTask = new JLabel(task.getTaskName());
		this.tasksPanel.add(thisTask);
		this.tasksPanel.revalidate();
		this.tasksPanel.repaint();
		return index;
	}
	
	/**
	 * @param index
	 */
	public void RemoveTask(int index) {

		this.tasksPanel.remove(index);
		this.tasksPanel.revalidate();
		this.tasksPanel.repaint();
	}

	/**
	 * @param tasks
	 */
	public void SetTasks(ArrayList<Task> tasks) {
		this.noOfTasks = tasks.size();
		this.tasksPanel.removeAll();
		this.tasksPanel.revalidate();
		this.tasksPanel.repaint();
//		this.tasksPanel.add(Box.createHorizontalGlue());
		for (int i = 0; i < tasks.size(); i++) {
			JLabel thisTask = new JLabel(tasks.get(i).getTaskName());
			//thisTask.setBackground(Color.green);
//			thisTask.setSize(50, 30);
			this.tasksPanel.add(thisTask, BorderLayout.CENTER);
		}
//		this.tasksPanel.add(Box.createHorizontalGlue());
		this.tasksPanel.add(Box.createVerticalGlue());
		this.tasksPanel.revalidate();
		this.tasksPanel.repaint();
	}

	/**
	 * @return
	 */
	public int GetNoOfTasks() {
		return this.noOfTasks;
	}
}
