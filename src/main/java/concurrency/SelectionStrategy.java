package concurrency;

public enum SelectionStrategy {
	SHORTEST_QUEUE, SHORTEST_TIME;
}
