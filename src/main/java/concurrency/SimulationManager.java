package concurrency;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.Random;

import com.sun.security.ntlm.Server;

import model.Task;
import util.TaskComparator;
import util.Utils;
import view.SimulatorFrame;

public class SimulationManager implements Runnable {
	// data read from UI
	public int numberOfClients = 200;
	public SelectionStrategy selectionPolicy = SelectionStrategy.SHORTEST_QUEUE;

	// entity responsible with queue management and client distribution
	private Scheduler schedule;
	// frame for displaying simulation
	private SimulatorFrame frame;
	// pool of tasks (client shopping in the store)
	private List<Task> generatedTasks;
	private List<Task> undispatchedTasks;
	private List<ServerQueue> servers;

	/**
	 * @param schedule
	 * @param frame
	 */
	public SimulationManager(Scheduler schedule, SimulatorFrame frame) {

		this.schedule = schedule;
		this.frame = frame;
		schedule.changedStrategy(selectionPolicy);
		this.generateNRandomTasks();

	}

	/**
	 * 
	 */
	private void generateNRandomTasks() {

		generatedTasks = new ArrayList<Task>();
		undispatchedTasks = new ArrayList<Task>();
		int N = Utils.Rand(0, numberOfClients);// (Math.random() * numberOfClients + 1);
		System.out.println(N);
		int lastArrivalTime = this.frame.GetOpeningTime();
		for (int i = 0; i < N; i++) {
			int processingTime = Utils.Rand(this.frame.GetMinService(), this.frame.GetMaxService());
			int timeFromLastArrival = Utils.Rand(this.frame.GetMinArriving(), this.frame.GetMaxArriving());
			int arrivalTime = lastArrivalTime + timeFromLastArrival;
			Task t = new Task(arrivalTime, 0, processingTime, i);
//			t.setWaitingTime(lastArrivalTime + lastServiceTime - arrivalTime); ///
//			t.setFinishTime(arrivalTime+processingTime+t.getWaitingTime()); ///
			generatedTasks.add(t);
//			System.out.println("Arrival time: " + arrivalTime + "; Service time: " + processingTime);
			lastArrivalTime = arrivalTime;

		}
		Collections.sort(generatedTasks, new TaskComparator());
		// generate N random tasks;
		// - random processing time
		// minProcessingTime < processingTime < maxProcessingTime
		// - random arrivalTime
		// sort list with respect to arrivalTime
	}

	/* (non-Javadoc)
	 * @see java.lang.Runnable#run()
	 */
	public void run() {
		int currentTime = this.frame.GetOpeningTime();
		Utils.CurrentTime = currentTime;
		this.schedule.startServers();
		int serviceAvailable = currentTime + this.frame.GetServiceAvailable();
		while (currentTime < serviceAvailable) {
//			Iterator<Task> iterator = generatedTasks.iterator();
			for (Task t : generatedTasks) {
//				Task t = iterator.next();
				if (t.getArrivalTime() == currentTime) {
					schedule.dispatchTask(t);
				} else {
					undispatchedTasks.add(t);
				}
			}
			generatedTasks.clear();
			generatedTasks = undispatchedTasks;
			undispatchedTasks = new ArrayList<Task>();
			currentTime++;
			Utils.CurrentTime = currentTime;
			try {
				Thread.sleep(Utils.TimeRatio);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}

		System.out.println("The servers are closed.");
		this.schedule.stopServers();
		while (!this.schedule.CheckServers()) {
		}
		for (ServerQueue server : schedule.getServerQueues()) {
			server.summary();

		}
	}
}
