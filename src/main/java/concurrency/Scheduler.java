package concurrency;

import java.util.ArrayList;
import java.util.List;

import model.Task;
import view.SimulatorFrame;

public class Scheduler {

		private List<ServerQueue> servers;
		private int maxNoServerQueues = 3;
		private Strategy strategy;
		private SimulatorFrame frame;
		
		/**
		 * @param maxNoServerQueues
		 * @param frame
		 */
		public Scheduler(int maxNoServerQueues, SimulatorFrame frame) {
			this.maxNoServerQueues = maxNoServerQueues;
			this.frame = frame;
			createServers();
		}

		/**
		 * 
		 */
		private void createServers() {
			servers = new ArrayList<ServerQueue>();
			for (int i = 0; i < maxNoServerQueues; i++) {
				servers.add(new ServerQueue(this.frame, i));
			}
		}
		/**
		 * 
		 */
		public void startServers() {
			for(ServerQueue s: servers) {
				new Thread(s).start();
			}
		}
		
		/**
		 * 
		 */
		public void stopServers() {
			for(ServerQueue s : servers) {
				s.setState(false);
			}
		}
		/**
		 * @param policy
		 */
		public void changedStrategy(SelectionStrategy policy) {
			
			if(policy == SelectionStrategy.SHORTEST_QUEUE) {
				strategy = new ConcreteStrategyQueue();
			}
			if(policy == SelectionStrategy.SHORTEST_TIME) {
				strategy = new ConcreteStrategyTime();
			}
		}
		
		/**
		 * @param t
		 */
		public void dispatchTask(Task t) {
			strategy.addTask(servers, t);
		}
		
		/**
		 * @return
		 */
		public List<ServerQueue> getServerQueues(){
			return servers;
		}
		
		/**
		 * @return
		 */
		public Boolean CheckServers() {
			Boolean allStoped = true;
			for(ServerQueue s : servers) {
				if(!s.flag && allStoped) {
//					System.out.println(s.name + " still running");
					allStoped = false;
				}
			}
			return allStoped;
		}
}
