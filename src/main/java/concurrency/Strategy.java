package concurrency;

import java.util.List;

import model.Task;

public interface Strategy {
	
	/**
	 * @param servers
	 * @param t
	 */
	public void addTask(List<ServerQueue> servers, Task t);
}
