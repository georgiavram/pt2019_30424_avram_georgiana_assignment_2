package concurrency;

import java.util.List;

import model.Task;

public class ConcreteStrategyTime implements Strategy {

	/* (non-Javadoc)
	 * @see concurrency.Strategy#addTask(java.util.List, model.Task)
	 */
	public void addTask(List<ServerQueue> servers, Task t) {
		// TODO Auto-generated method stub
		ServerQueue destination = servers.get(0);
		int min = 9999;
		int sum = 0;
		for(ServerQueue s: servers) {
			for(Task j: s.getTasks()) {
				sum += j.getServiceTime();
			}
			if(min > sum) {
				min = sum;
				destination = s;
				System.out.println("ptt");
			}
			
		}
		destination.addTask(t);
	}

}
