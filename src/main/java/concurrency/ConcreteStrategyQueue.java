package concurrency;

import java.util.List;

import model.Task;

public class ConcreteStrategyQueue implements Strategy {

	/* (non-Javadoc)
	 * @see concurrency.Strategy#addTask(java.util.List, model.Task)
	 */
	public void addTask(List<ServerQueue> servers, Task t) {
		// TODO Auto-generated method stub
		ServerQueue destination = servers.get(0);
		int min = servers.get(0).getNrOfTasks();
		for(ServerQueue s: servers) {
			if(min > s.getNrOfTasks()) {
				min = s.getNrOfTasks();
				destination = s;
			}
		}
		destination.addTask(t);
	}

}
