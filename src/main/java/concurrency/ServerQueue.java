
package concurrency;

import java.awt.Color;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.NoSuchElementException;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.HashMap;
import java.util.Map;

import javax.swing.JButton;

import model.Task;
import util.Utils;
import view.ServerObj;
import view.SimulatorFrame;

public class ServerQueue implements Runnable {
	private ArrayList<Task> finishedTasks = new ArrayList<Task>();
	protected ArrayList<Task> tasks;
	protected int lastLeavingTime;
	private SimulatorFrame frame;
	private ServerObj server;
	public String name;
	private int index;
	private Boolean isRunning = true;
	public Boolean flag = false;

	/**
	 * @param frame
	 * @param index
	 */
	public ServerQueue(SimulatorFrame frame, int index) {
		this.frame = frame;
		this.index = index;
		this.tasks = new ArrayList<Task>();
//		this.waitingPeriod = new AtomicInteger(0);
		this.name = "Server" + index;
		this.server = new ServerObj(this.name);

		// initialize queue and waitingPeriod;
	}

	/**
	 * @param newTask
	 */
	public void addTask(Task newTask) {
//		System.out.println(lastLeavingTime);
//		newTask.setWaitingReference(lastLeavingTime);
//		newTask.displayIndex = this.server.AddTask(newTask);
		this.tasks.add(newTask);
//		waitingPeriod.incrementAndGet();
		this.server.SetTasks(this.tasks);
		this.frame.updateSever(this.server, this.index);
		System.out.println("Task added: " + newTask.GetName(false));
		// add task to queue
		// increment the waitingPeriod
	}

	/**
	 * @param isRunning
	 */
	public void setState(Boolean isRunning) {
		this.isRunning = isRunning;
	}

	/**
	 * @return
	 */
	public Boolean IsRunning() {
		return isRunning;
	}

	/* (non-Javadoc)
	 * @see java.lang.Runnable#run()
	 */
	public void run() {

		this.frame.addServer(this.server, this.index);
		while (this.isRunning) {
//			 System.out.println(getNrOfTasks());

			try {

				if (getNrOfTasks() > 0) {
					Task t = this.tasks.get(0);

//				t.setWaitingTime(waitingPeriod - t.getServerTimePeriod());
					try {
						Thread.sleep(t.getServiceTime() * Utils.TimeRatio);
						this.tasks.remove(0);
						this.server.SetTasks(this.tasks);
						this.frame.updateSever(this.server, this.index);
						finishedTasks.add(t);
						t.setFinishTime(Utils.CurrentTime);
//						lastLeavingTime = t.getLeavingTime();

						System.out.println("Task finished: " + t.GetName(true) + " ;waiting time: " + t.getWaitingTime());
					} catch (InterruptedException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
					
				} else {
					Thread.sleep(10);
				}
			} catch (NoSuchElementException exc) {

			} catch (InterruptedException e) {
			}

		}
		System.out.println("Server closed: " + this.name);
		this.frame.closeServer(this.index);
		this.flag = true;
	}

	/**
	 * @return
	 */
	public float getAverageProcessingTime() {
		if (finishedTasks.size() == 0) {
			return -1;
		}
		float service = 1;
		for (Task tk : finishedTasks) {
			service += tk.getServiceTime();
		}
		return service / finishedTasks.size();
	}

	/**
	 * @return
	 */
	public ArrayList<Task> getTasks() {
		return tasks;
	}

	/**
	 * @return
	 */
	public float getAverageWaitingTime() {
		if (finishedTasks.size() == 0) {
			return -1;
		}
		int average = 1;
		for (Task tk : finishedTasks) {
			average += tk.getWaitingTime();// getWaitingTime(tk);
		}
		return average / finishedTasks.size();
	}

//	public int getWaitingTime(Task t) {
//		double time = 0;
//		for (Task tk : finishedTasks) {
//			if (!(tk.equals(t))) {
//				if (tk.getLeavingTime() != 0 && tk.getLeavingTime() > t.getArrivalTime()) {
//
//					time += (tk.getLeavingTime() - t.getArrivalTime()); // am aici un bai
//
//				}
//			}
//		}
//		return 0;
//	}

	/**
	 * @return
	 */
	public int getPeakHour() {
		HashMap<Integer, Integer> map = new HashMap<Integer, Integer>();
		for (Task t : finishedTasks) {
			int h = t.getArrivalTime() / 100;
			if (map.containsKey(h)) {
				int newValue = map.get(h) + 1;
				map.put(h, newValue);
			} else {
				map.put(h, 1);
			}
		}
		int max = 0;
		int hour = -1;
		for (Integer value : map.keySet()) {
			if (map.get(value) > max) {
				max = map.get(value);
				hour = value;
			}
		}
		return hour;
	}

	/**
	 * 
	 */
	public void summary() {
		System.out.println("\n Here is the summary for " + name + ":\nAverage waiting time: " + getAverageWaitingTime()
				+ "\n Average service time:" + getAverageProcessingTime() + "\n Peak hour: " + getPeakHour() + "\n");
	}

	/**
	 * @return
	 */
	public int getNrOfTasks() {
		return tasks.size();
	}
}
