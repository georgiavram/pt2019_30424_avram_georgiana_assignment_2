package util;

import java.util.Comparator;

import model.Task;

public class TaskComparator implements Comparator<Task> {

	/* (non-Javadoc)
	 * @see java.util.Comparator#compare(java.lang.Object, java.lang.Object)
	 */
	public int compare(Task o1, Task o2) {
		// TODO Auto-generated method stub
		if (o1.getArrivalTime() < o2.getArrivalTime())
			return -1;
		else if (o1.getArrivalTime() > o2.getArrivalTime())
			return 1;
		return 0;
	}

}
