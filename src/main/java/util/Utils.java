package util;

import java.util.Random;

public class Utils {
	
	public static int TimeRatio = 200;
	public static int CurrentTime = 0;
	
	/**
	 * @param a
	 * @param b
	 * @return
	 */
	public static int Rand(int a, int b) {

		Random rand = new Random();
		int value;
		do {
			value = rand.nextInt(100000);
		} while ((value < a) || (value > b));
		return value;
	}
	
	/**
	 * @param time
	 * @return
	 */
	public static String GetTimeString(int time) {
		int hours = time/60;
		int minutes = time % 60;
		return hours + ":" + minutes;
	}
}
