package model;

import util.Utils;

public class Task {
	private int arrivalTime;
	private int finishTime;
	private int serviceTime;
	private int waitingTime;
	private int waitingReference;
	public String taskName;
	public int index;
	public int displayIndex;

	/**
	 * @param arrivalTime
	 * @param finishTime
	 * @param serviceTime
	 * @param index
	 */
	public Task(int arrivalTime, int finishTime, int serviceTime, int index) {
		this.arrivalTime = arrivalTime;
		this.finishTime = finishTime;
		this.serviceTime = serviceTime;
		this.index = index;
		this.taskName = "Client " + index + " (" + Utils.GetTimeString(arrivalTime) + ")";
	}

	/**
	 * @return
	 */
	/**
	 * @return
	 */
	public int getArrivalTime() {
		return this.arrivalTime;
	}

	/**
	 * @param arrivalTime
	 */
	public void setArrivalTime(int arrivalTime) {
		this.arrivalTime = arrivalTime;
	}

	/**
	 * @return
	 */
	public int getFinishTime() {
		return this.finishTime;
	}

	/**
	 * @param finishTime
	 */
	public void setFinishTime(int finishTime) {
		this.finishTime = finishTime;
	}

	/**
	 * @return
	 */
	public int getServiceTime() {
		return this.serviceTime;
	}

	/**
	 * @param serviceTime
	 */
	public void setServiceTime(int serviceTime) {
		this.serviceTime = serviceTime;
	}

	/**
	 * @param isFinished
	 * @return
	 */
	public String GetName(Boolean isFinished) {
		return this.taskName + (isFinished ? " (" + Utils.GetTimeString(this.getLeavingTime()) + ")" : "");
	}

	/**
	 * @return
	 */
	public int getWaitingTime() {
		return this.finishTime - this.arrivalTime - this.serviceTime;
	}

	/**
	 * @return
	 */
	public String getTaskName() {
		return taskName;
	}

	/**
	 * @return
	 */
	public int getLeavingTime() {

		return this.arrivalTime + this.waitingTime + this.serviceTime;

	}
}
